module.exports = {
  theme: {
    extend: {
			colors: {
				text: "#cccccc",
				bgBody: "#1e1e1e",
				blockquoteBorder: "#333333",
				blockquoteText: "#666666",
				bgInlineCode: "#333333",
				linkArticle: "#666666",

				gray: {
					"1e": "#1e1e1e",
					"333": "#333333",
					"3f": "#3f3f3f",
					"555": "#555555",
					"888": "#888888",
					"aaa": "#aaaaaa"
				}
			}
		},
		fontFamily: {
			global: ['Arial']
		},
		listStyleType: {
			disc: "disc",
			circle: "circle",
			decimal: "decimal"
		},
	},
	variants: ['responsive', 'last']
}
