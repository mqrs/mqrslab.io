module.exports = {
	siteName: 'MQRS',
	templates: {
		Article: "/articles/:fileName"
	},
	plugins: [
		{
			use: '@gridsome/source-filesystem',
			options: {
				typeName: "Article",
				path: "./collections/articles/*.md"
			}
		}
	],
	css: {
		loaderOptions: {
			postcss: {
				plugins: [
					require("tailwindcss"),
					...process.env.NODE_ENV === "production" ? [require("@fullhuman/postcss-purgecss")] : []
				]
			}
		}
	},
	transformers: {
		remark: {
			// global remark options
		}
	},
	chainWebpack: config => {
    const svgRule = config.module.rule('svg')
    svgRule.uses.clear()
    svgRule
      .use('vue-svg-loader')
      .loader('vue-svg-loader')
  }
}
